<?php

namespace Drupal\mailing_subscriber;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

interface SubscriberInterface extends ConfigurableInterface, PluginInspectionInterface, PluginFormInterface {

  /**
   * Get the label.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Subscribe the given email address.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function subscribe(FormStateInterface $form_state);

  /**
   * Build the form the end-user uses to subscribe to your mailing list
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function buildForm(array &$form, FormStateInterface $form_state);
}
