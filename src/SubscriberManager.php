<?php

namespace Drupal\mailing_subscriber;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\mailing_subscriber\Annotation\Subscriber;

class SubscriberManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/mailing_subscriber/Subscriber',
      $namespaces,
      $module_handler,
      SubscriberInterface::class,
      Subscriber::class
    );
    $this->alterInfo('mailing_subscriber_subscribers');
    $this->setCacheBackend($cache_backend, 'mailing_subscriber_subscribers');
  }

  public function getPluginDefinitionByType($type) {
    $definitions = array_filter(
      $this->getDefinitions(),
      function ($definition) use ($type) {
        if (!empty($definition['types']) && in_array(
            $type,
            $definition['types']
          )) {
          return TRUE;
        }
        return FALSE;
      }
    );
    return $definitions;
  }

  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'broken';
  }

  public function getVisibleDefinitions() {
    $definitions = $this->getDefinitions();
    unset($definitions['broken']);
    return $definitions;
  }

  /**
   * @param $plugin_id
   * @param array $configuration
   *
   * @return \Drupal\mailing_subscriber\SubscriberInterface
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return parent::createInstance($plugin_id, $configuration);
  }

}
